#Coursework 55

#Analysis of suicide rates in India(2001-2012), identifying different major factors influencing 
#the suicide rates and discussion of different results.


library(ggplot2) 
library(plotrix)
library(readr) 
library(dplyr)
library(highcharter)
library(stringr)
library(ggpubr)
library(Hmisc)
library(devtools)
library(aod)

Input = "H:/Msc AI/Team_Research&Dev/Final_Sub/Suicides in India 2001-2012.csv"
#Read csv - source file
sc_data<-read.csv(Input)
str(sc_data)
#Summarise the dataset
summary(sc_data)

#define the color combination to use 
color<-c("blue","red","gray5","lawngreen","green","orangered1","lightskyblue","orange","palevioletred1","springgreen","yellow","tan1","red")

#######################################################################################

#Who suicide more male or female? 
grpTot<-sc_data %>% 
  select(Gender,Total)%>% 
  group_by(Gender)%>% 
  summarise(sum_total=sum(Total))%>%
  mutate(rs=sum(sum_total), percent=round((sum_total/rs)*100))

#bar chart - What percentage of Male and Female suicide? 
ggplot(grpTot,aes(x=Gender,y=percent,fill=Gender))+
  geom_bar(stat="identity")+
  scale_fill_manual(values=c("dodgerblue","mediumorchid1"))+
  geom_text(aes(label=percent))


#pie chart - What percentage of Male and Female suicide?
label <-  c( paste(grpTot$Gender[1],grpTot$percent[1],'%',sep=''),
             paste(grpTot$Gender[2],grpTot$percent[2],'%',sep=''))
pie3D(grpTot$percent,labels=label,labelcex=1.5,explode=0.1,col=color)

#*** It is clearly seen that 64% of total population male than 36% female suicide
#    for different reasons. Here we are going to study mainly two factors that influence 
#    the people to suicide - Farming and Education ***#

#######################################################################################


#Overall statistical analysis for male and female
gpTot<-sc_data %>% filter(!Type_code =="Means_adopted"
                          & !Type %in% c("Causes Not known",
                                         "Middle",
                                         "Student",
                                         "Others (Please Specify)",
                                         "House Wife"))%>%
  select(Type_code,Type,Gender,Total)%>% 
  group_by(Type,Gender)%>% 
  summarise(su_total=sum(Total))

#bar chart - What percentage of Male and Female suicide? 
ggplot(gpTot,aes(x=Type,y=su_total,,fill=Gender))+
  geom_bar(stat="identity",position="dodge")+
  scale_fill_manual(values=c("dodgerblue","mediumorchid1"))+
  theme(legend.position = "bottom",
        axis.text.x = element_text(angle=90,
                                   hjust=0.95,
                                   vjust=0.2))

#######################################################################################


#Suicide statistics corresponding to the Education_Status
edu_type<-sc_data %>% 
  filter(Type_code =="Education_Status"&
           !Type %in% c("Middle"))%>%
  group_by(Type,Gender,Age_group)%>%
  summarise(edutot=sum(Total))

edu_type%>%
  ggplot(aes(x=Type,y=edutot,label=edutot,fill=Gender))+
  geom_text(aes(label=edutot),
            check_overlap = FALSE,
            position = position_dodge(0.9),
            vjust = 0)+
  geom_bar(stat="identity",position="dodge")+
  scale_fill_manual(values=color)+
  theme(legend.position = "bottom",
        axis.text.x = element_text(angle=90,
                                   hjust=0.95,
                                   vjust=0.2))+
  labs(x="Education_Status",y="Suicide Count")

#Box plot
edu_type %>% 
  ggplot(aes(x=str_sub(Type,1,15),y=edutot,fill=Type))+
  geom_boxplot()+
  stat_summary(fun.y=mean, geom="point", shape=20, size=3, color="white", fill="red")+
  scale_fill_manual(values=color)+
  theme(legend.position = "bottom",
        axis.text.x = element_text(angle=90,
                                   hjust=0.95,
                                   vjust=0.2))+
  labs(x="Education Level",y="Suicide Count")

#*** We are considering the Education is one of the prior factor, as this will
#    take the country forward. Hence we can see that most of the students committing 
#    suicides in their "Primary Schooling" and in which most are male candidates ***#


#######################################################################################


#Suicide statistics corresponding to the Farming/Agriculture
far_type<-sc_data %>% 
  filter((Type_code =="Professional_Profile" 
          & Type=="Farming/Agriculture Activity"))%>%
  group_by(Type,Gender,Age_group)%>%
  summarise(fartot=sum(Total))


far_type%>%
  ggplot(aes(x=Type,y=fartot,label=fartot,fill=Gender))+
  geom_text(aes(label=fartot),
            check_overlap = TRUE,
            position = position_dodge(0.9),
            vjust = -0.3)+
  geom_bar(stat="identity",position="dodge")+
  scale_fill_manual(values=color)+
  theme(legend.position = "bottom",
        axis.text.x = element_text(angle=0))+
  labs(x="Farming/Agriculture Activity",y="Suicide Count")

ggplot(far_type,aes(x=Type,y=fartot,fill=Age_group))+
  geom_bar(stat = "identity",
           width = 0.7)+
  scale_x_discrete(name="Professional Profile") + 
  scale_y_continuous(breaks=seq(0,400000,50000),
                     name = "Suicide Count")+
  facet_wrap(~Gender)+
  theme(axis.text.x = element_text(angle=0))+
  theme(legend.position="bottom")+
  theme(axis.line = element_line(color = "black",
                                 size=1.25))+
  theme(panel.background=element_blank())

#Box plot
far_type %>% 
  ggplot(aes(x=str_sub(Type,1,15),y=fartot,fill=Type))+
  geom_boxplot()+
  #geom_jitter()+
  stat_summary(fun.y=mean, geom="point", shape=20, size=5, color="red", fill="red")+
  scale_fill_manual(values=color[3])+
  theme(legend.position = "bottom",
        axis.text.x = element_text(angle=0))+
  labs(x="Farming/Agriculture Activity",y="Suicide Count")

#*** We are considering farming as this the major occupation in India and it contribute
#    considerate amount to India's GDP. We can see from the graph that mostly farmers
#    within age group 30-44 are committing suicides and in which most are males whereas
#    female farmers commits suicide within age group of 15-29 years ***#

#######################################################################################

#Other main cause for suicides
other_Cause <- sc_data%>%
  filter(Type_code=="Causes")%>%
  group_by(Type,Gender)%>%
  summarise(oTotal=sum(Total))
hchart(other_Cause,type="column",hcaes(x = Type, y = oTotal, group = Gender)) %>%
  hc_add_theme(hc_theme_google()) %>%
  hc_credits(enabled = TRUE, text = "Data Source: National Crime Records Bureau (NCRB), Govt of India ", style = list(fontSize = "14px")) %>%
  hc_legend(enabled = TRUE)%>%
  hc_xAxis(title = list(text = "Other Reasons for Suicide"))%>%
  hc_yAxis(title = list(text = "Total"))

#*** After ploting the data point we can see that the most prominent reason of suicide 
#    other than farming and education is family problems, where both male and female 
#    suicide are comparatively high to other factors in India ***#

#######################################################################################

#How scattered is the male to female suicide in different causes associated
main_cause<-sc_data %>% 
  filter(Type_code =="Causes")%>% 
  group_by(Type,Gender,Age_group)%>%
  summarise(ctot=sum(Total))
main_cause%>%
  ggplot(aes(x=Type,y=ctot,color=Gender))+
  geom_point()+
  scale_color_manual(values=color)+
  theme(legend.position = "bottom",axis.text.x = element_text(angle=90,vjust=0.5))+
  labs(x="Causes",y="Suicide Count")

#*** Though the suicide counts is not that scattered but is subsequently high comapred
#    to other factors. We can also see that people suicide for unknown reasons and also
#    for other reasons. Females can be seen suiciding more due to Dowry Dispute, 
#    Illegitimate pregnancy, Physical Abuse and Suspected/Illicit Relationships ***#

#######################################################################################

#What is the mean adopted method of suicide?
mean_Adopted<- sc_data%>%
  filter(Type_code=="Means_adopted")%>%
  group_by(Type,Gender,Age_group)%>%
  summarise(mTotal=sum(Total))
ggplot(mean_Adopted,aes(x=Type,y=mTotal,fill=Age_group))+
  geom_bar(stat = "identity",
           width = 0.7)+
  scale_x_discrete(name="Means of adoption for suicide") + 
  scale_y_continuous(breaks=seq(0,400000,50000),
                     name = "Suicide count")+
  facet_wrap(~Gender)+
  theme(axis.text.x = element_text(angle=90,
                                   hjust=0.95,
                                   vjust=0.2))+
  theme(legend.position="bottom")+
  theme(axis.line = element_line(color = "black",
                                 size=1.25))+
  theme(panel.background=element_blank())


#*** We can clearly see that suicide rate among the males are extremly high as compared 
#    to females, most men suicide by hanging themselves or by consuming insecticides or 
#    poison. We can see a common trend between male and female suiciding by hanging 
#    themselves or by consuming in insecticides or poison. It is also seen that most men
#    between age of 30-44 year are prone to suicide where as females between 15-29 
#    years has more probability of suicides***#


#######################################################################################

#How the trend line of suicide within Farmers have prevailed over years?
sc_data%>% filter(Type_code=="Professional_Profile" & 
                    Type=="Farming/Agriculture Activity")%>%
  select(Type_code,Year,Total,Type)%>% 
  group_by(Year,Type_code)%>%
  summarise(ytot=sum(Total))%>% 
  
  ggplot(aes(x=factor(Year),
             y=ytot,
             color=Type_code,
             group=Type_code))+
  geom_line(size=1)+
  geom_text(aes(label = ytot),
            vjust = "inward", 
            hjust = "outward",
            show.legend = FALSE) +
  scale_color_manual(values=color)+
  theme(legend.position = "bottom",
        axis.text.x = element_text(angle=65,
                                   vjust=0.5))+
  labs(x="Year",y="Suicide Count")+
  geom_point(size=2)

#*** It is seen that the suicide rate in India among the farmer was highest in 2004
#    but significant decrease is seen from 2009 from 17368 to 13754 suicides ***#


#How the trend line of suicide within students have prevailed over years?
sc_data %>% filter(Type_code=="Education_Status")%>%
  select(Type_code,Year,Total,Type)%>% 
  group_by(Year,Type_code)%>%
  summarise(ltot=sum(Total))%>%  
  
  ggplot(aes(x=factor(Year),
             y=ltot,
             color=Type_code,
             group=Type_code))+
  geom_line(size=1)+
  geom_text(aes(label = ltot),
            vjust = "inward", 
            hjust = "outward",
            show.legend = FALSE) +
  scale_color_manual(values=color)+
  theme(legend.position = "bottom",
        axis.text.x = element_text(angle=65,
                                   vjust=0.5))+
  labs(x="Year",y="Suicide Count")+
  geom_point(size=2)

#*** It is seen that the suicides among student has a significant rise from 
#    2005 from 113914 to 135585 in 2011(recorded as the highest) ***#

#######################################################################################


#How many suicide are committed by different age group in Professional Profile
P_f_type<-sc_data %>% filter(Type_code =="Professional_Profile" &
                               !Type %in% c("House Wife","Others (Please Specify)"))%>%
  group_by(Type,Gender,Age_group)%>%
  summarise(pftot=sum(Total))

P_f_type%>%
  ggplot(aes(x=Type,y=pftot,label=pftot,fill=Gender))+
  geom_bar(stat="identity",position="dodge")+
  scale_fill_manual(values=color)+
  theme(legend.position = "bottom",
        axis.text.x = element_text(angle=90,
                                   hjust=0.95,
                                   vjust=0.2))+
  labs(x="Professional_Profile",y="Suicide Count")

#Separate distinction of Male and female suicide
ggplot(P_f_type,aes(x=Type,
                    y=pftot,
                    fill=Age_group))+
  geom_bar(stat = "identity",
           width = 0.7)+
  scale_x_discrete(name="Professional Profile") + 
  scale_y_continuous(breaks=seq(0,400000,50000),
                     name = "Suicide count")+
  facet_wrap(~Gender)+
  theme(axis.text.x = element_text(angle=90,
                                   hjust=0.95,
                                   vjust=0.2))+
  theme(legend.position="bottom")+
  theme(axis.line = element_line(color = "black",
                                 size=1.25))+
  theme(panel.background=element_blank())

#Box plot
P_f_type %>% 
  ggplot(aes(x=str_sub(Type,1,15),
             y=pftot,
             fill=Type))+
  geom_boxplot()+
  stat_summary(fun.y=mean, 
               geom="point", 
               shape=20, 
               size=3, 
               color="white", 
               fill="red")+
  scale_fill_manual(values=color)+
  theme(legend.position = "bottom",
        axis.text.x = element_text(angle=90))+
  labs(x="Professional Profile",y="Suicide Count")

#*** The main Professional profile of most Indian are farming and most suicide are 
#    committed among the major professional porfolio.

#######################################################################################

#What is the age group that commit more suicides?
sc_data$Year<-as.factor(sc_data$Year)
sc_data%>%filter(!Age_group %in% c("0-100+"))%>%
  select(Year,Total,Age_group) %>% 
  group_by(Year,Age_group) %>% 
  summarise(total=sum(Total))%>%
  ggplot(aes(x=Year,
             y=total,
             group=Age_group,
             fill=Age_group))+
  geom_area()+
  scale_fill_manual(values=colr)+
  theme(legend.position = "bottom",
        axis.text.x = element_text(angle=90))

#*** It is pictured that age group between 15-29 and 30-44 years has the more 
#    tentitive case of committing suicides, 0-14 years being the in least number.
#    It is also seen that there has been an increase in suicide rate among 15-29
#    years from 2009-2010 and dropped from 2011-2012***#

#######################################################################################


#Trending suicide rate per year in respect to Profession and education
sc_data %>% filter((Type_code %in% c("Professional_Profile","Education_Status"))&
                     Type %in% c("Farming/Agriculture Activity",
                                 "Primary",
                                 "No Education",
                                 "Post Graduate and Above",
                                 "Graduate",
                                 "Hr. Secondary/Intermediate/Pre-Universit",
                                 "Diploma", 
                                 "Matriculate/Secondary"))%>%
  select(Year,Total,Type)%>% 
  group_by(Year,Type)%>%
  summarise(ytot=sum(Total))%>%  
  ggplot(aes(x=factor(Year),y=ytot,color=Type,group=Type))+
  geom_line(size=1)+
  geom_point(size=2)+
  scale_color_manual(values=color)+
  theme(legend.position = "bottom",
        axis.text.x = element_text(angle=65,
                                   vjust=0.5))+
  labs(x="Year",y="Suicide Count")

#*** It is clearly visible that the suicides in farmer has decreased over years but 
#    suicide among students in every section has increased yearly ***#

#######################################################################################


#Year wise suicide in each state
sc_data%>%select(State,Year,Age_group,Total) %>% 
  group_by(State,Year,Age_group)%>% 
  summarise(sttot=sum(Total)) %>% 
  
  ggplot(aes(x=State,
             y=sttot,
             fill=Year))+
  geom_bar(stat="identity")+
  theme(legend.position="bottom",
        axis.text.x=element_text(angle=90))+
  labs(x="States",y="Suicide Counts")

#*** Maharashtra has seen more suicides over 12 years than any other states.
#    Andhra Pradesh, West Bengal and Tamil Nadu has seen a similar suicide rate
#    over years. Lakshadweep shows a suicide rate 1 in million people. ***# 


#Tile graph
sc_data %>%filter(!State %in% c("Total (All India)",
                                "Total (States)",
                                "Total (Uts)"))%>%
  ggplot(aes(x=factor(Year),
             y=State,fill=Total))+
  geom_tile()+
  facet_wrap(~Gender)+
  scale_fill_gradientn(colors=brewer.pal(5,"Blues"))+
  theme(axis.text.x = element_text(angle=90))+
  labs(x="Year")

#*** From the tile graph it is seen that females in West Bengal were suiciding more
#    than other states till 2005 and Maharastra till 2002. In case of male in West Bengal
#    prominent increase in suicide from 2004-2007 and then decreased and eventually 
#    increased from 2011. In case of Tamil Nadu and Karnataka the male suicide remains 
#    constant for years and then started decreasing. Maharastra and Andhra Pradesh has 
#    depicted more male suicides over years to almost 8000 numbers.

#######################################################################################


#Total_Population of each state to total suicide numbers in each state
#in case of farmers and students
sc_data%>%select(State,Type_code,Type,Year,Total,Total_Population) %>% 
  group_by(State,Type_code,Type)%>%
  summarise(xtot=sum(Total))
sc_data%>%filter((Type_code %in% c("Professional_Profile","Education_Status"))&
                   Type %in% c("Farming/Agriculture Activity",
                               "Primary",
                               "No Education",
                               "Post Graduate and Above",
                               "Graduate",
                               "Hr. Secondary/Intermediate/Pre-Universit",
                               "Diploma",
                               "Matriculate/Secondary"))%>%
  
  ggplot(aes(x=(Total_Population/10),
             y=Total))+
  geom_point(stat="identity")+
  geom_smooth(method="auto", 
              se=TRUE, 
              fullrange=TRUE, 
              level=0.95)+
  theme(legend.position="bottom",
        axis.text.x=element_text(angle=90,
                                 hjust=0.95,
                                 vjust=0.2))+
  labs(x="Total Population(million)",
       y="Suicide Counts")

#*** By this it is seen that more the population or least the population
#    least is the suicide rates ***#

#######################################################################################

sc_data<-read.csv(Input)

#Correlation coefficient of the year to total suicides
df<-sc_data %>%
  select(Year,Total)%>% 
  group_by(Year)%>%
  summarise(ftot=sum(Total))


#Correlation coefficient of the year to total suicides committed by farmers
sc_data<-read.csv(Input)
df<-sc_data %>% 
  filter((Type_code %in% c("Professional_Profile"))&
                   Type %in% c("Farming/Agriculture Activity"))%>%
  select(Year,Total)%>% 
  group_by(Year)%>%
  summarise(ftot=sum(Total))


#Correlation coefficient of the year to total suicides committed by students
sc_data<-read.csv(Input)
df<-sc_data %>% 
  filter((Type_code %in% c("Education_Status"))&
           Type %in% c("Primary",
                       "No Education",
                       "Post Graduate and Above",
                       "Graduate",
                       "Hr. Secondary/Intermediate/Pre-Universit",
                       "Diploma",
                       "Matriculate/Secondary"))%>%
  select(Year,Total)%>% 
  group_by(Year)%>%
  summarise(ftot=sum(Total))


#Pearson
ggscatter(df, 
          x = "Year", 
          y = "ftot", 
          add = "reg.line", 
          conf.int = TRUE, 
          cor.coef = TRUE,
          cor.method = "pearson",
          xlab = "Year", 
          ylab = "Total Suicides")

#Result 1- R=0.98,p=8.2e-07 - Total Suicide correlated to preceeding Years
#Result 2- R=-0.73,p=0.0066 - Total farmer Suicide correlated to preceeding Years
#Result 3- R=0.97,p=2.5e-07 - Total student Suicide correlated to preceeding Years


#Spearman
ggscatter(df, 
          x = "Year", 
          y = "ftot", 
          add = "reg.line", 
          conf.int = TRUE, 
          cor.coef = TRUE,
          cor.method = "spearman",
          xlab = "Year", 
          ylab = "Total Suicides")
#Result 1- R=0.98,p<2.2e-16 - Total Suicide correlated to preceeding Years
#Result 2- R=-0.66,p=0.022 - Total farmer Suicide correlated to preceeding Years
#Result 3- R=0.99,p<2.2e-16 - Total student Suicide correlated to preceeding Years

#Kendall
ggscatter(df, 
          x = "Year", 
          y = "ftot", 
          add = "reg.line", 
          conf.int = TRUE, 
          cor.coef = TRUE, 
          cor.method = "kendall",
          xlab = "Year", 
          ylab = "Total Suicides")
#Result 1- R=0.94,p=3.2e-07 - Total Suicide correlated to preceeding Years
#Result 2- R=-0.58,p=0.0088 - Total farmer Suicide correlated to preceeding Years
#Result 3- R=0.94,p=3.2e-07 - Total student Suicide correlated to preceeding Years



#######################################################################################

#Find p-value
sc_data<-read.csv(Input)
df<-sc_data %>% 
  filter((Type_code %in% c("Professional_Profile"))&
           Type %in% c("Farming/Agriculture Activity"))%>%
  select(Year,Total,Age_group)
#df
#***********************************************************************************
sc_data<-read.csv(Input)
df<-sc_data %>% 
  filter((Type_code %in% c("Education_Status"))&
           Type %in% c("Primary",
                       "No Education",
                       "Post Graduate and Above",
                       "Graduate",
                       "Hr. Secondary/Intermediate/Pre-Universit",
                       "Diploma",
                       "Matriculate/Secondary"))%>%
  select(Year,Total,Age_group)
#df
#***********************************************************************************

#Significance of total suicide to Age group and Year
df$Total <- factor(df$Total)
mylogit <- glm(Total ~ Age_group, data = df, family = "binomial")
summary(mylogit)
confint(mylogit)
confint.default(mylogit)

#Chi-Square
wald.test(b = coef(mylogit), Sigma = vcov(mylogit), Terms = 4:5)

exp(coef(mylogit))
exp(cbind(OR = coef(mylogit), confint(mylogit)))

with(mylogit, null.deviance - deviance)
with(mylogit, df.null - df.residual)
#p-value
with(mylogit, pchisq(null.deviance - deviance, df.null - df.residual, lower.tail = FALSE))
logLik(mylogit)

#######################################################################################


#Total suicide to Year wise plotting with regression line

sc_data %>% 
  select(Year,Total)%>% 
  group_by(Year)%>%
  summarise(ftot=sum(Total))
reg1 <- lm(Total~Year,data=sc_data) 
summary(reg1)


df<-sc_data %>% 
  select(Year,Total)%>% 
  group_by(Year)%>%
  summarise(ftot=sum(Total))
df
reg2 <- lm(ftot~Year,data=df) 
summary(reg2)

with(df,plot(Year, ftot,xlab="Years", ylab="Total suicide counts"))
abline(reg2)

#######################################################################################
