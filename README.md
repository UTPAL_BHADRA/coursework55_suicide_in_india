---
Author: Coursework 55
		UTPAL BHADRA – 18044201
		SAURAV KUMAR BASU – 17059082
		BLAISEY AMROJ SOJ – 18049645
		JOYCE NAA ADJELEY ANDERSON ANNAN – 18053132
		SOLOMON FRANCIS OKOLO – 18051696
Course: 7COM1079
Date: 20.12.2019
Institute: University of Hertfordshire
Module: 7COM1079
Subtitle: '7COM1079 -- Team Research and Development Project'
Term: 'fall-19'
Title: 
What major factors influence the increase rate of suicide in India and 
how can these commonalities be used to aid the government in curtailing the rate of suicide?
---


# README #

Analysis on dataset of Suicide in India from 2001-2012 and answering of research question - 
"What major factors influence the increase rate of suicide in India and 
how can these commonalities be used to aid the government in curtailing the rate of suicide?"

### What is this repository for? ###

* Analysis of suicide rates in India and what preventive measures can be taken by the government as well as individuals
* Version - 1.0
* Bit Bucket - git clone https://UTPAL_BHADRA@bitbucket.org/UTPAL_BHADRA/coursework55_suicide_in_india.git
* Kaggle dataset - https://www.kaggle.com/utpalbhadra93/suicide-in-india-with-population-20012012
* Trello - https://trello.com/b/pbVXkV05/coursework-55

### How do I get set up? ###

* git clone https://UTPAL_BHADRA@bitbucket.org/UTPAL_BHADRA/coursework55_suicide_in_india.git to a prefered directory
* Download and Install R - https://cran.ma.imperial.ac.uk/
* Download and Install RStudio - https://www.google.co.in/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&cad=rja&uact=8&ved=2ahUKEwju05KZlcTmAhXJMMAKHYXTAx0QFjAAegQICRAC&url=https%3A%2F%2Frstudio.com%2Fproducts%2Frstudio%2Fdownload%2F&usg=AOvVaw1u3lNXvtFzIREO5HoDgQxZ
* Open the script in R and install the libraries 
* Change the "Input" variable to the path the downloaded dataset path
* Click the "Run" button in RStudio to get the code run for the output

### Who do I talk to? ###

* Coursework 55 
* Contact Details
Name - UTPAL BHADRA
Email - ub19aab@herts.ac.uk